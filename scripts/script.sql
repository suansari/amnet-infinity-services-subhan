CREATE TABLE commercial_input_store
(
   id           INT            NOT NULL AUTO_INCREMENT,
   create_date  DATETIME,
   domain       VARCHAR(255),
   isactive     INT,
   priority     INT,
   profile_id   INT,
   update_date  DATETIME,
   PRIMARY KEY (id)
);

CREATE TABLE profile
(
   profile_id    INT           NOT NULL AUTO_INCREMENT,
   profile_name  VARCHAR(50),
   PRIMARY KEY (profile_id)
);

CREATE TABLE spc_computation_output_store
(
   id                         INT            NOT NULL AUTO_INCREMENT,
   domain                     VARCHAR(100),
   recommended_cpm            DOUBLE,
   expected_ctr               DOUBLE,
   expected_viewability       VARCHAR(10),
   expected_impressions       BIGINT,
   profile_id                 INT,
   recommendation_identifier  INT,
   campaign_id        BIGINT,
   campaign_type      VARCHAR(255),
   creation_date      DATETIME,
   is_active          int NOT NULL,
   PRIMARY KEY (id)
);

CREATE TABLE trader_input_store
(
   id                 INT            NOT NULL AUTO_INCREMENT,
   advertiser_id      BIGINT,
   budget             BIGINT,
   campaign_id        BIGINT,
   campaign_type      VARCHAR(255),
   creative_size      VARCHAR(255),
   ctr_goal           VARCHAR(255),
   device_type        VARCHAR(255),
   start_date         DATE,
   end_date           DATE,
   total_impressions  BIGINT,
   viewability_goal   VARCHAR(255),
   profile_id         INT,
   PRIMARY KEY (id)
);

ALTER TABLE spc_computation_output_store
  ADD CONSTRAINT spc_computation_output_store_ibfk_1 FOREIGN KEY (profile_id)
  REFERENCES profile (profile_id);


ALTER TABLE trader_input_store
  ADD CONSTRAINT trader_input_store_ibfk_1 FOREIGN KEY (profile_id)
  REFERENCES profile (profile_id);


ALTER TABLE commercial_input_store
  ADD CONSTRAINT commercial_input_store_ibfk_1 FOREIGN KEY (profile_id)
  REFERENCES profile (profile_id);


