package com.miq.infinity.controller.test;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.miq.infinity.controller.LookupController;
import com.miq.infinity.controller.RestDispatcher;
import com.miq.infinity.engine.LookupService;
import com.miq.infinity.entity.LookupStore;

@RunWith(SpringRunner.class)
@SpringBootTest
public class LookupControllerTest {

	private static final Log LOGGER = LogFactory.getLog(LookupController.class);

	public static final MediaType APPLICATION_JSON_UTF8 = new MediaType(MediaType.APPLICATION_JSON.getType(),
			MediaType.APPLICATION_JSON.getSubtype(), Charset.forName("utf8"));

	public static class Uri {
		public static final String LOOKUPS = "/lookups";
		public static final String LOOKUPS_LIST = LOOKUPS + "/list";
	}

	@InjectMocks
	private LookupController lookupController;
	
	
	private MockMvc mockMvc;

	@Mock
	private LookupService lookupService;

	@Before
	public void init() {
		MockitoAnnotations.initMocks(this);
		mockMvc = MockMvcBuilders.standaloneSetup(lookupController).build();
	}

	@Test
	public void exampleTest() {
		List<LookupStore> lookups = new ArrayList<LookupStore>();
		LookupStore ls = new LookupStore();
		ls.setName("L1");
		ls.setDescription("D1");
		lookups.add(ls);

		when(lookupService.getAllLookups()).thenReturn(lookups);
		try {
			mockMvc.perform(get(RestDispatcher.SERVICE_VERSION_1 + Uri.LOOKUPS_LIST).contentType(APPLICATION_JSON_UTF8))
					.andExpect(status().isOk());
		} catch (Exception e) {
			LOGGER.error(e);

		}
	}

}
