/*
 * Author : Subhan Ansari
 */
package com.miq.infinity.controller;

import org.springframework.stereotype.Component;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@Component
public class RestDispatcher {

	public static final String SERVICE_VERSION_1 = "/service/v1";
		
}
