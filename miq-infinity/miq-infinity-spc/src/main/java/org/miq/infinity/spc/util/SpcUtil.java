package org.miq.infinity.spc.util;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.miq.infinity.spc.entity.CommercialInputStore;
import org.miq.infinity.spc.exception.BadRequestException;
import org.miq.infinity.spc.exception.DataNotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import com.opencsv.CSVReader;

@Component
@Configuration
@PropertySource("classpath:application.properties")
public class SpcUtil {

    private static final Logger LOG = LoggerFactory.getLogger(SpcUtil.class);
    public static final char FILE_SEPERATOR = ',';
    public static final String FILE_EMPTY_MESSAGE = "File is Empty, Please upload correct file";
    public static final String FILE_TYPE_MESSAGE = "Please upload .csv or .xlsx file";
    public static final String FILE_SUCCESS_MESSAGE = "File is uploaded and saved successfully in database";

    private List<CommercialInputStore> parseUploadedExcelFile(MultipartFile uploadFile, int profileId)
            throws IOException {
        List<CommercialInputStore> commInputList = new ArrayList<>();

        Date recordDate = new Date();

        try (InputStream inputStream = new BufferedInputStream(uploadFile.getInputStream());
                XSSFWorkbook workbook = new XSSFWorkbook(inputStream);) {
            Sheet firstSheet = workbook.getSheetAt(0);
            Iterator<Row> iterator = firstSheet.iterator();

            while (iterator.hasNext()) {
                Row nextRow = iterator.next();
                if (nextRow.getRowNum() > 0) {
                    Iterator<Cell> cellIterator = nextRow.cellIterator();

                    CommercialInputStore commInput = new CommercialInputStore();

                    while (cellIterator.hasNext()) {
                        Cell nextCell = cellIterator.next();
                        int columnIndex = nextCell.getColumnIndex();

                        switch (columnIndex) {
                        case 0:
                            commInput.setDomain((String) getCellValue(nextCell));
                            break;
                        case 1:
                            double value = (Double) getCellValue(nextCell);
                            int value2 = (int) value;
                            commInput.setPriority(value2);
                            break;
                        default:
                            break;
                        }
                    }
                    commInput.setProfileId(profileId);
                    commInput.setCreateDate(recordDate);
                    commInput.setUpdateDate(recordDate);
                    commInput.setIsActive(1);
                    commInputList.add(commInput);
                }
            }
        } catch (FileNotFoundException e) {
            LOG.error("FileNotFoundException::" + e.getMessage());
        } catch (IOException e) {
            LOG.error("IOException::" + e.getMessage());
        }
        return commInputList;
    }

    private Object getCellValue(Cell cell) {

        switch (cell.getCellTypeEnum()) {
        case STRING:
            return cell.getRichStringCellValue().getString();
        case BOOLEAN:
            return cell.getBooleanCellValue();
        case NUMERIC:
            return cell.getNumericCellValue();
        case BLANK:
            break;
        case ERROR:
            break;
        case FORMULA:
            break;
        case _NONE:
            break;
        default:
            break;
        }

        return FILE_SUCCESS_MESSAGE;
    }

    private List<CommercialInputStore> parseUploadedCsvFile(MultipartFile uploadFile, int profileId)
            throws IOException {
        Date recordDate = new Date();

        List<CommercialInputStore> commInputList = new ArrayList<>();

        try (CSVReader reader = new CSVReader(new InputStreamReader(uploadFile.getInputStream()), FILE_SEPERATOR);) {

            List<String[]> records = reader.readAll();

            Iterator<String[]> iterator = records.iterator();
            // skip header row
            iterator.next();

            while (iterator.hasNext()) {
                String[] record = iterator.next();

                CommercialInputStore commInput = new CommercialInputStore();

                commInput.setDomain(record[0]);
                commInput.setPriority(Integer.parseInt(record[1]));
                commInput.setProfileId(profileId);
                commInput.setCreateDate(recordDate);
                commInput.setUpdateDate(recordDate);
                commInput.setIsActive(1);

                commInputList.add(commInput);
            }

        } catch (FileNotFoundException e) {
            LOG.error("FileNotFoundException::" + e.getMessage());
            throw e;
        } catch (IOException e) {
            throw e;
        }
        return commInputList;
    }

    public String computeSpcAlgorithm(String commInputToJson, String traderInputToJson, String pythonScriptPath)
            throws DataNotFoundException {

        String spcComputationOutput = null;
        String spcCompute = null;

        String[] cmd = new String[4];
        cmd[0] = "python";
        cmd[1] = pythonScriptPath;
        cmd[2] = commInputToJson;
        cmd[3] = traderInputToJson;

        Runtime rt = Runtime.getRuntime();
        Process pr;
        try {
            pr = rt.exec(cmd);
            BufferedReader bfr = new BufferedReader(new InputStreamReader(pr.getInputStream()));

            if (bfr.ready()) {
                while ((spcComputationOutput = bfr.readLine()) != null) {
                    LOG.info("spc output is {}", spcComputationOutput);
                    spcCompute = spcComputationOutput;
                }
            } else {
                LOG.error("SPC computation is empty for script :: {}", pythonScriptPath);
                throw new DataNotFoundException("SPC computation output not found");
            }
        } catch (Exception e) {
            LOG.error("Error in reading python output for :: {}", pythonScriptPath);
        }
        return spcCompute;
    }

    public List<CommercialInputStore> parseUploadedFile(MultipartFile uploadFile, int profileId) {
        List<CommercialInputStore> commercialInputStoreList = null;
        try {
            if (uploadFile.getSize() == 0) {
                LOG.error("File is having no contents");
                throw new BadRequestException("Bad Request 400, Please upload file with contents");
            } else {
                String fileType = uploadFile.getOriginalFilename().split("\\.")[1];
                LOG.info("Commercial Input file type :: {}", fileType);

                if (fileType.equals("csv")) {
                    commercialInputStoreList = parseUploadedCsvFile(uploadFile, profileId);

                } else if (fileType.equals("xlsx")) {
                    commercialInputStoreList = parseUploadedExcelFile(uploadFile, profileId);
                } else {
                    LOG.info("Commercial Input file type is not csv or excel " + uploadFile.getOriginalFilename());
                    throw new BadRequestException("Bad Request 400, Please upload file with .xlsx or .csv extention");
                }
            }
        } catch (Exception e) {
            LOG.error("Error in uploading file" + uploadFile.getOriginalFilename());
        }
        return commercialInputStoreList;
    }

    public List<CommercialInputStore> parseFileAndreturnCommercialInputsList(MultipartFile uploadFile, int profileId) {
        List<CommercialInputStore> commercialInputStoreList = parseUploadedFile(uploadFile, profileId);
        if (commercialInputStoreList == null) {
            LOG.error("Issue in uploading file {} ::", uploadFile.getOriginalFilename());
        }
        return commercialInputStoreList;
    }

    public boolean isCollectionEmpty(Collection<?> collection) {
        if (collection == null || collection.isEmpty()) {
            return true;
        }
        return false;
    }
}
