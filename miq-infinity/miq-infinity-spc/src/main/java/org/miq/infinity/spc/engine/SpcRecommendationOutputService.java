package org.miq.infinity.spc.engine;

import java.util.List;
import org.miq.infinity.spc.domain.TraderInputCriteriaImpl;
import org.miq.infinity.spc.entity.CommercialInputStore;
import org.miq.infinity.spc.entity.SpcComputationOutputStore;
import org.miq.infinity.spc.repository.CommercialInputStoreDao;
import org.miq.infinity.spc.repository.SpcComputationOutputRepository;
import org.miq.infinity.spc.util.SpcUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.fasterxml.jackson.databind.ObjectMapper;

@Transactional
@Service
public class SpcRecommendationOutputService {

    private static final Logger LOG = LoggerFactory.getLogger(SpcRecommendationOutputService.class);

    @Autowired
    SpcCommercialService spcCommercialService;

    @Autowired
    CommercialInputStoreDao commercialInputStoreDao;

    @Autowired
    private SpcComputationOutputRepository spcComputationOutputRepository;

    @Autowired
    private Environment environment;

    @Autowired
    private SpcUtil spcUtil;

    private static final String DATA_SUCCESS = "SUCCESS";
    private static final String DATA_ERROR = "ERROR";

    public String computeTraderInput(TraderInputCriteriaImpl traderInputCriteria, int profileId) {
        String commInputToJson = null;
        String traderInputToJson = null;
        String status = null;
        String pythonScriptPath = environment.getProperty("commercial.python.script.path");

        LOG.info("SPC computation script path :: {}", pythonScriptPath);

        ObjectMapper objectMapper = new ObjectMapper();
        List<CommercialInputStore> list = spcCommercialService.getAllCommercialInput();

        try {
            commInputToJson = objectMapper.writeValueAsString(list);
            traderInputToJson = objectMapper.writeValueAsString(traderInputCriteria);
            LOG.info("Commercial input for spc computation is {}", commInputToJson);
            LOG.info("Trader input for spc computation is {}", traderInputToJson);
            String spcOutput = spcUtil.computeSpcAlgorithm(commInputToJson, traderInputToJson, pythonScriptPath);

            if (spcOutput != null) {
                LOG.info("SPC computation output :: {}", spcOutput);
                status = parseSpcOutput(spcOutput, profileId, traderInputCriteria);
            } else {
                status = DATA_ERROR;
            }

        } catch (Exception e) {
            LOG.error("Error computing in trader input {}", traderInputCriteria.toString());
        }
        return status;
    }

    public String parseSpcOutput(String spcOutput, int profileId, TraderInputCriteriaImpl traderinputcriteria) {

        int isActive = 0;
        String status = null;
        int updatedRecommendationId = 0;

        try {
            updateExistingRecord(isActive);

            String recommendationId = spcComputationOutputRepository.getRecommendationIdentifier();

            if (recommendationId == null) {
                updatedRecommendationId = 1;

            } else {
                updatedRecommendationId = Integer.parseInt(recommendationId);
                updatedRecommendationId = updatedRecommendationId + 1;
            }

            LOG.info(" updated recommendationId with {}", updatedRecommendationId);

            status = spcComputationOutputRepository.saveSpcOutput(spcOutput, profileId, isActive, traderinputcriteria,
                    updatedRecommendationId);
            LOG.info("SPC output {} :: {}", spcOutput, status);

        } catch (Exception e) {
            LOG.error("SPC output {} :: {}", spcOutput, status);
        }
        return status;
    }

    public List<SpcComputationOutputStore> getAllSpcOutput(int isActive) {
        return spcComputationOutputRepository.getAllSpcOutputList(isActive);
    }

    public void updateExistingRecord(int isActive) {
        spcComputationOutputRepository.updateSpcOutput(isActive);
    }
}
