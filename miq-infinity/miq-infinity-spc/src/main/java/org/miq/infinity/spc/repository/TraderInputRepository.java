package org.miq.infinity.spc.repository;

import org.miq.infinity.spc.domain.TraderInputCriteriaImpl;

public interface TraderInputRepository {

    public String saveTraderInput(TraderInputCriteriaImpl traderInputCriteria, int profileId, String creativeSize);

}
