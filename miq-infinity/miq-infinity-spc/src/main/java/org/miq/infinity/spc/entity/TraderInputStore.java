package org.miq.infinity.spc.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.stereotype.Component;

@Component
@Entity
@Table(name = "trader_input_store")
public class TraderInputStore {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @Column(name = "profile_id")
    private int profileId;

    @Column(name = "advertiser_id")
    private long advertiserId;

    @Column(name = "campaign_id")
    private long campaignId;

    @Column(name = "campaign_type")
    private String campaignType;

    @Column(name = "start_date")
    @Temporal(TemporalType.DATE)
    private Date startdate;

    @Column(name = "end_date")
    @Temporal(TemporalType.DATE)
    private Date enddate;

    @Column(name = "total_impressions")
    private long impressions;

    @Column(name = "budget")
    private long budget;

    @Column(name = "ctr_goal")
    private String ctrGoal;

    @Column(name = "viewability_goal")
    private String viewabilityGoal;

    @Column(name = "device_type")
    private String deviceType;

    @Column(name = "creative_size")
    private String creativeSize;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getProfileId() {
        return profileId;
    }

    public void setProfileId(int profileId) {
        this.profileId = profileId;
    }

    public long getAdvertiserId() {
        return advertiserId;
    }

    public void setAdvertiserId(long advertiserId) {
        this.advertiserId = advertiserId;
    }

    public long getCampaignId() {
        return campaignId;
    }

    public void setCampaignId(long campaignId) {
        this.campaignId = campaignId;
    }

    public String getCampaignType() {
        return campaignType;
    }

    public void setCampaignType(String campaignType) {
        this.campaignType = campaignType;
    }

    public Date getStartdate() {
        return startdate;
    }

    public void setStartdate(Date startdate) {
        this.startdate = startdate;
    }

    public Date getEnddate() {
        return enddate;
    }

    public void setEnddate(Date enddate) {
        this.enddate = enddate;
    }

    public long getImpressions() {
        return impressions;
    }

    public void setImpressions(long impressions) {
        this.impressions = impressions;
    }

    public long getBudget() {
        return budget;
    }

    public void setBudget(long budget) {
        this.budget = budget;
    }

    public String getCtrGole() {
        return ctrGoal;
    }

    public void setCtrGoal(String ctrGoal) {
        this.ctrGoal = ctrGoal;
    }

    public String getViewabilityGoal() {
        return viewabilityGoal;
    }

    public void setViewabilityGoal(String viewabilityGoal) {
        this.viewabilityGoal = viewabilityGoal;
    }

    public String getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(String deviceType) {
        this.deviceType = deviceType;
    }

    public String getCreativeSize() {
        return creativeSize;
    }

    public void setCreativeSize(String creativeSize) {
        this.creativeSize = creativeSize;
    }

    @Override
    public String toString() {
        return "TraderInputStrore [id=" + id + ", profileId=" + profileId + ", advertiserId=" + advertiserId
                + ", campaignId=" + campaignId + ", campaignType=" + campaignType + ", startdate=" + startdate
                + ", enddate=" + enddate + ", impressions=" + impressions + ", budget=" + budget + ", ctrGoal="
                + ctrGoal + ", viewabilityGoal=" + viewabilityGoal + ", deviceType=" + deviceType + ", creativeSize="
                + creativeSize + "]";
    }
}
