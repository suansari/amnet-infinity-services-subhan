package org.miq.infinity.spc.controller;

import java.util.List;

import org.miq.infinity.spc.domain.CommInputCreteriaImpl;
import org.miq.infinity.spc.domain.TraderInputCriteriaImpl;
import org.miq.infinity.spc.engine.SpcCommercialService;
import org.miq.infinity.spc.engine.SpcProfileService;
import org.miq.infinity.spc.engine.SpcRecommendationOutputService;
import org.miq.infinity.spc.engine.SpcTraderService;
import org.miq.infinity.spc.entity.CommercialInputStore;
import org.miq.infinity.spc.entity.SpcComputationOutputStore;
import org.miq.infinity.spc.repository.CommercialInputStoreDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import io.swagger.annotations.ApiOperation;

@RestController
@CrossOrigin
public class SpcController {

    private static final Logger LOG = LoggerFactory.getLogger(SpcController.class);

    @Autowired
    private SpcCommercialService spcCommercialService;

    @Autowired
    private SpcRecommendationOutputService spcRecommendationOutputService;

    @Autowired
    private SpcTraderService spcTraderService;

    @Autowired
    CommercialInputStoreDao commercialInputStoreDao;

    static final class Uri {
        private Uri() {
        }

        public static final String SPC = "/spc";
        public static final String COMM_LIST = SPC + "/media-owners/priority/list";
        public static final String COMM_INPUT = SPC + "/media-owners/add";
        public static final String COMM_INPUT_UPDATE = SPC + "/media-owners/update";
        public static final String COMM_INPUT_DELETE = SPC + "/media-owners/delete";
        public static final String TRADER_INPUT = SPC + "/campaigns/media-owners/add";
        public static final String SPC_OUTPUT_LIST = SPC + "/campaigns/media-owners/recommendation/list";
    }

    @ApiOperation(value = "Returns commercial inputs list uploaded by commercial team.")
    @RequestMapping(value = Uri.COMM_LIST, method = RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE })
    public ResponseEntity<List<CommercialInputStore>> getAllCommInput() {
        LOG.info("Getting all comm input");
        ResponseEntity<List<CommercialInputStore>> responseEntity = new ResponseEntity<>(
                spcCommercialService.getAllCommercialInput(), HttpStatus.OK);
        LOG.info("Exiting SpcController after listing commercial input");
        return responseEntity;
    }

    @ApiOperation(value = "Upload commercialInput file and persist it in DB so that it can be shown in infinity when user sees SPC Media Owner Priorities page.")
    @RequestMapping(value = Uri.COMM_INPUT, headers = "Content-Type= multipart/form-data", method = RequestMethod.POST)
    public ResponseEntity<String> uploadCommercialInput(@RequestParam("file") MultipartFile uploadFile,
            @RequestParam("profilename") String profileName) {
        String statusMessage = null;
        ResponseEntity<String> response = null;
        try {
            LOG.info("Uploading commercial input {} file", uploadFile.getOriginalFilename());
            statusMessage = spcCommercialService.saveCommInput(uploadFile, profileName);
        } catch (Exception e) {
            LOG.error("Problem in upload CommercialInput for file :: {}", uploadFile.getOriginalFilename());
        }
        response = new ResponseEntity<>(uploadFile.getOriginalFilename() + statusMessage + "-", HttpStatus.OK);
        LOG.info("Exiting SpcController after uploading {} commercial input: {} ", uploadFile.getOriginalFilename());
        return response;
    }

    @ApiOperation(value = "Update commercial input")
    @RequestMapping(value = Uri.COMM_INPUT_UPDATE, method = RequestMethod.PUT, consumes = {
            MediaType.APPLICATION_JSON_VALUE })
    public ResponseEntity<String> updateCommercialInput(@RequestBody CommInputCreteriaImpl commInputCriteria) {
        ResponseEntity<String> response = null;
        String updatedRecord = null;
        try {
            LOG.info("updating commercial input for {} ", commInputCriteria.getDomain());
            updatedRecord = spcCommercialService.updateCommercialInput(commInputCriteria);
        } catch (Exception e) {
            LOG.error("Problem in updating commercial input", e.getMessage());
        }
        response = new ResponseEntity<>(updatedRecord + "-", HttpStatus.OK);
        return response;
    }

    @ApiOperation(value = "Delete commercial input")
    @RequestMapping(value = Uri.COMM_INPUT_DELETE, method = RequestMethod.DELETE)
    public ResponseEntity<String> deleteCommercialInput(@RequestParam("id") int id) {

        ResponseEntity<String> response = null;
        String deleteRecord = null;

        try {
            LOG.info("deleting commercial input for {} ", id);
            deleteRecord = spcCommercialService.deleteCommInput(id);
        } catch (Exception e) {
            LOG.error("Problem in deleting commercial input", e);
            throw e;
        }
        response = new ResponseEntity<>(deleteRecord + "for Id" + id + "-", HttpStatus.OK);
        return response;
    }

    @ApiOperation(value = "Upload and Compute trader input")
    @RequestMapping(value = Uri.TRADER_INPUT, method = RequestMethod.POST, consumes = {
            MediaType.APPLICATION_JSON_VALUE })
    public ResponseEntity<String> uploadTraderInput(@RequestBody TraderInputCriteriaImpl traderinputcriteria) {
        ResponseEntity<String> response = null;
        String status = "";
        try {
            LOG.info("uploading and computing spc for username{} ", traderinputcriteria.getProfileName());
            status = spcTraderService.uploadTraderInput(traderinputcriteria);
        } catch (Exception e) {
            LOG.error("problem in uploading and computing spc for username{} ", traderinputcriteria.getProfileName());
            throw e;
        }
        response = new ResponseEntity<>(status, HttpStatus.OK);
        return response;
    }

    @ApiOperation(value = "Return spc output list")
    @RequestMapping(value = Uri.SPC_OUTPUT_LIST, method = RequestMethod.GET, produces = {
            MediaType.APPLICATION_JSON_VALUE })
    public ResponseEntity<List<SpcComputationOutputStore>> getAllSpcList() {
        int isActive = 1;
        LOG.info("Getting all spc computation");
        ResponseEntity<List<SpcComputationOutputStore>> responseEntity = new ResponseEntity<>(
                spcRecommendationOutputService.getAllSpcOutput(isActive), HttpStatus.OK);
        return responseEntity;
    }
}