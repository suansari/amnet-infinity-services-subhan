package org.miq.infinity.spc.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.stereotype.Component;

@Component
@Entity
@Table(name = "commercial_input_store")

public class CommercialInputStore {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @Column(name = "profile_id")
    private int profileId;

    @Column(name = "domain")
    private String domain;

    @Column(name = "priority")
    private int priority;

    @Column(name = "create_date")
    // @Temporal(TemporalType.DATE)
    private Date createDate;

    @Column(name = "update_date")
    // @Temporal(TemporalType.DATE)
    private Date updateDate;

    @Column(name = "isactive")
    private int isActive;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public int getIsActive() {
        return isActive;
    }

    public void setIsActive(int isActive) {
        this.isActive = isActive;
    }

    public int getProfileId() {
        return profileId;
    }

    public void setProfileId(int profileId) {
        this.profileId = profileId;
    }

    @Override
    public String toString() {
        return "CommercialInputStore [id=" + id + ", profileId=" + profileId + ", domain=" + domain + ", priority="
                + priority + ", createDate=" + createDate + ", updateDate=" + updateDate + ", isActive=" + isActive
                + "]";
    }

}
