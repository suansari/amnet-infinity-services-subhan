package org.miq.infinity.spc.domain;

import java.util.Date;
import java.util.List;

public class TraderInputCriteriaImpl implements TraderInputCriteria {

    private String profileName;
    private long advertiserId;
    private long campaignId;
    private String campaignType;
    private Date startdate;
    private Date enddate;
    private long impressions;
    private long budget;
    private String ctrGoal;
    private String viewabilityGoal;
    private String deviceType;
    private List<String> creativeSize;

    public List<String> getCreativeSize() {
        return creativeSize;
    }

    public void setCreativeSize(List<String> creativeSize) {
        this.creativeSize = creativeSize;
    }

    public String getCampaignType() {
        return campaignType;
    }

    public void setCampaignType(String campaignType) {
        this.campaignType = campaignType;
    }

    public void setAdvertiserId(long advertiserId) {
        this.advertiserId = advertiserId;

    }

    public void setCampaignId(long campaignId) {
        this.campaignId = campaignId;
    }

    public void setStartdate(Date startdate) {
        this.startdate = startdate;
    }

    public void setEnddate(Date enddate) {
        this.enddate = enddate;
    }

    public void setImpressions(long impressions) {
        this.impressions = impressions;
    }

    public void setBudget(long budget) {
        this.budget = budget;
    }

    public void setDeviceType(String deviceType) {
        this.deviceType = deviceType;
    }

    public void setProfileName(String profileName) {
        this.profileName = profileName;
    }

    public void setCtrGoal(String ctrGoal) {
        this.ctrGoal = ctrGoal;
    }

    public void setViewabilityGoal(String viewabilityGoal) {
        this.viewabilityGoal = viewabilityGoal;
    }

    @Override
    public long getAdvertiserId() {
        return advertiserId;
    }

    @Override
    public long getCampaignId() {
        return campaignId;
    }

    @Override
    public Date getStartdate() {
        return startdate;
    }

    @Override
    public Date getEnddate() {
        return enddate;
    }

    @Override
    public long getImpressions() {
        return impressions;
    }

    @Override
    public long getBudget() {
        return budget;
    }

    @Override
    public String getCtrGoal() {
        return ctrGoal;
    }

    @Override
    public String getViewabilityGoal() {
        return viewabilityGoal;
    }

    @Override
    public String getDeviceType() {
        return deviceType;
    }

    @Override
    public String getProfileName() {
        return profileName;
    }

    @Override
    public String toString() {
        return "TraderInputCriteriaImpl [profileName=" + profileName + ", advertiserId=" + advertiserId
                + ", campaignId=" + campaignId + ", campaignType=" + campaignType + ", startdate=" + startdate
                + ", enddate=" + enddate + ", impressions=" + impressions + ", budget=" + budget + ", ctrGoal="
                + ctrGoal + ", viewabilityGoal=" + viewabilityGoal + ", deviceType=" + deviceType + ", creativeSize="
                + creativeSize + "]";
    }

}
