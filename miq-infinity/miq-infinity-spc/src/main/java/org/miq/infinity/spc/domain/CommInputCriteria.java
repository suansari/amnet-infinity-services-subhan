package org.miq.infinity.spc.domain;

public interface CommInputCriteria {

    public int getId();

    public int getPriority();

    public String getDomain();
}
