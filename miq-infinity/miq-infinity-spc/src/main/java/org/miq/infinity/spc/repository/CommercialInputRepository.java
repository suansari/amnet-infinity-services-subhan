package org.miq.infinity.spc.repository;

import java.util.List;

import org.miq.infinity.spc.domain.CommInputCreteriaImpl;
import org.miq.infinity.spc.entity.CommercialInputStore;

import com.fasterxml.jackson.core.JsonProcessingException;

public interface CommercialInputRepository {

    public String updateCommercialRecord(CommInputCreteriaImpl commInputCriteria) throws JsonProcessingException;

    public String deleteCommercialRecord(int id);

    public String saveCommInputData(List<CommercialInputStore> commercialInputStoreList);

    public List<CommercialInputStore> getCommInputList();
}
