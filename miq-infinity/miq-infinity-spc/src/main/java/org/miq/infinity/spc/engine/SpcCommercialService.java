package org.miq.infinity.spc.engine;

import java.util.ArrayList;
import java.util.List;

import org.miq.infinity.spc.domain.CommInputCreteriaImpl;
import org.miq.infinity.spc.entity.CommercialInputStore;
import org.miq.infinity.spc.exception.ResourceNotFoundException;
import org.miq.infinity.spc.repository.CommercialInputRepository;
import org.miq.infinity.spc.repository.CommercialInputStoreDao;
import org.miq.infinity.spc.util.SpcUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.core.JsonProcessingException;

@Transactional
@Service
public class SpcCommercialService {

    private static final Logger LOG = LoggerFactory.getLogger(SpcCommercialService.class);

    @Autowired
    SpcProfileService spcProfileService;

    @Autowired
    SpcUtil spcUtil;

    @Autowired
    CommercialInputRepository commercialInputRepository;

    public List<CommercialInputStore> getAllCommercialInput() {
        List<CommercialInputStore> commInputList = commercialInputRepository.getCommInputList();
        if (spcUtil.isCollectionEmpty(commInputList)) {
            LOG.error("Commercial input list is null :: {} ", commInputList);
            throw new ResourceNotFoundException("Commercial input list not found");
        }
        return commInputList;
    }

    public String saveCommInput(MultipartFile uploadfile, String profileName) {
        String status = "";
        String commInput = "";
        List<CommercialInputStore> commercialInputStoreList = new ArrayList<>();
        int profileId = spcProfileService.getProfileId(profileName);
        LOG.info("Saved profile for {}:: in database with id {}:: ", profileName);
        try {
            commercialInputStoreList = spcUtil.parseFileAndreturnCommercialInputsList(uploadfile, profileId);
            commInput = commercialInputStoreList.toString();
            LOG.info("commercialInputStoreList is :: {}", commInput);

            if (!spcUtil.isCollectionEmpty(commercialInputStoreList)) {
                // Calling function to save data in to table
                status = commercialInputRepository.saveCommInputData(commercialInputStoreList);
                LOG.info("{}-Commercial Input-{}", commInput, status);
            } else {
                status = "Error in uploading and saving file";
                LOG.error("{}-Commercial Input-{}", commInput, status);
            }
        } catch (Exception e) {
            LOG.error("{} Commercial Input, {}", commercialInputStoreList.toString(), status);
        }
        return status;
    }

    public String updateCommercialInput(CommInputCreteriaImpl commInputCriteria) throws JsonProcessingException {
        return commercialInputRepository.updateCommercialRecord(commInputCriteria);
    }

    public String deleteCommInput(int id) {
        return commercialInputRepository.deleteCommercialRecord(id);
    }
}
