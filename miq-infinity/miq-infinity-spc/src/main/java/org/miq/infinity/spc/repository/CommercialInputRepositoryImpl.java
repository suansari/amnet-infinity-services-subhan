package org.miq.infinity.spc.repository;

import java.util.Date;
import java.util.List;
import org.miq.infinity.spc.domain.CommInputCreteriaImpl;
import org.miq.infinity.spc.entity.CommercialInputStore;
import org.miq.infinity.spc.exception.ResourceNotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Repository;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@Repository
public class CommercialInputRepositoryImpl implements CommercialInputRepository {

    private static final Logger LOG = LoggerFactory.getLogger(CommercialInputRepositoryImpl.class);

    private static final String DATA_DELETION_MESSAGE = "Data deleted successfully";

    private static final String DATA_SUCCESS = "Data saved successfully";

    @Autowired
    CommercialInputStoreDao commercialInputStoreDao;

    public String updateCommercialRecord(CommInputCreteriaImpl commInputCriteria) throws JsonProcessingException {
        CommercialInputStore commercialinputstore = new CommercialInputStore();
        ObjectMapper objectMapper = new ObjectMapper();
        String commInputToJson = null;
        Date updateDate = new Date();
        try {
            int id = commInputCriteria.getId();

            commercialinputstore = commercialInputStoreDao.findById(id);

            if (commercialinputstore == null) {
                LOG.error("Commercial input not found for id:: {}", id);
                throw new ResourceNotFoundException("Not Found, Record not found for id");
            } else {

                commercialinputstore.setDomain(commInputCriteria.getDomain());
                commercialinputstore.setPriority(commInputCriteria.getPriority());
                commercialinputstore.setUpdateDate(updateDate);

                commercialInputStoreDao.save(commercialinputstore);
            }

        } catch (NullPointerException e) {
            LOG.error("Problem in saving data into database for domain {}", commInputCriteria.getDomain());
            throw e;
        }
        commInputToJson = objectMapper.writeValueAsString(commercialinputstore);
        return commInputToJson;
    }

    @Override
    public String deleteCommercialRecord(int id) {
        try {
            commercialInputStoreDao.deleteById(id);
        } catch (Exception e) {
            LOG.error("Problem in deleting record, Try AGain for id {}", id);
            throw e;
        }
        return DATA_DELETION_MESSAGE;
    }

    @Override
    public String saveCommInputData(List<CommercialInputStore> commercialInputStoreList) {
        try {
            commercialInputStoreDao.deleteAll();
            commercialInputStoreDao.save(commercialInputStoreList);
        } catch (Exception e) {
            LOG.error("Problem in saving record : {}", commercialInputStoreList.toString());
            throw e;
        }
        return DATA_SUCCESS;
    }

    @Override
    public List<CommercialInputStore> getCommInputList() {
        List<CommercialInputStore> commercialInputStoreList = null;
        try {
            commercialInputStoreList = commercialInputStoreDao.findAll();
        } catch (ResourceNotFoundException e) {
            LOG.error("Problem in listing record");
        }
        return commercialInputStoreList;
    }
}