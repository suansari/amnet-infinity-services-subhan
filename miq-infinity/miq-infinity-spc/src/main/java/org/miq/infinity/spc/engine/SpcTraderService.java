package org.miq.infinity.spc.engine;

import org.miq.infinity.spc.domain.TraderInputCriteriaImpl;
import org.miq.infinity.spc.repository.CommercialInputStoreDao;
import org.miq.infinity.spc.repository.TraderInputRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@Service
public class SpcTraderService {

    private static final Logger LOG = LoggerFactory.getLogger(SpcTraderService.class);

    @Autowired
    CommercialInputStoreDao commercialInputStoreDao;

    @Autowired
    SpcRecommendationOutputService spcRecommendationOutputService;

    @Autowired
    SpcProfileService spcProfileService;

    @Autowired
    TraderInputRepository traderInputRepository;

    public String uploadTraderInput(TraderInputCriteriaImpl traderInputCriteria) {

        String status = "";
        String profileName = traderInputCriteria.getProfileName();
        String creativeSize = traderInputCriteria.getCreativeSize().toString();
        
        int profileId = spcProfileService.getProfileId(profileName);

        String traderInput = traderInputCriteria.toString();

        LOG.info("Started spc computation for {}", traderInput);

        status = spcRecommendationOutputService.computeTraderInput(traderInputCriteria, profileId);

        if (status.equals("SUCCESS")) {
            status = traderInputRepository.saveTraderInput(traderInputCriteria, profileId, creativeSize);

            LOG.info("Trader input :: {} :: {}", traderInput, status);
        }
        return status;
    }
}
