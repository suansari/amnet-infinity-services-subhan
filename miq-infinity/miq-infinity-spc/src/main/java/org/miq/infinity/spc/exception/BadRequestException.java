package org.miq.infinity.spc.exception;

public class BadRequestException extends Exception {

    /**
     * 
     */
    private static final long serialVersionUID = 4285102939497561697L;

    public BadRequestException(String message) {
        super(message);

    }

    public BadRequestException(String message, Throwable cause) {
        super(message);

    }

}
