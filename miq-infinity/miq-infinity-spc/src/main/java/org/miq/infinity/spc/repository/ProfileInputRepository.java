package org.miq.infinity.spc.repository;

public interface ProfileInputRepository {

    public int getAndSaveProfile(String profileName);

}
