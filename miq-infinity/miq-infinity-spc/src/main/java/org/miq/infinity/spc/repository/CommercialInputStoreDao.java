package org.miq.infinity.spc.repository;

import org.miq.infinity.spc.entity.CommercialInputStore;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CommercialInputStoreDao extends JpaRepository<CommercialInputStore, Long> {

    public CommercialInputStore findById(int id);

    public void deleteById(int id);
}
