package org.miq.infinity.spc.exception;

public class ResourceNotFoundException extends RuntimeException {

    /**
     * 
     */
    private static final long serialVersionUID = -8524797934168365751L;

    public ResourceNotFoundException(String message) {
        super(message);
    }
}
