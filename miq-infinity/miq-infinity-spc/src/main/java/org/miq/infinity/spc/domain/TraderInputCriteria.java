package org.miq.infinity.spc.domain;

import java.util.Date;
import java.util.List;

public interface TraderInputCriteria {

    public String getProfileName();

    public long getAdvertiserId();

    public long getCampaignId();

    public Date getStartdate();

    public Date getEnddate();

    public long getImpressions();

    public long getBudget();

    public String getCtrGoal();

    public String getViewabilityGoal();

    public String getDeviceType();

    public List<String> getCreativeSize();

}
