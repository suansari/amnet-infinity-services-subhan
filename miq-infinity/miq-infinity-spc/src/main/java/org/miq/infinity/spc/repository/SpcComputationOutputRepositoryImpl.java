package org.miq.infinity.spc.repository;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.miq.infinity.spc.domain.SpcComputationOutputCriteria;
import org.miq.infinity.spc.domain.SpcComputationOutputCriteriaImpl;
import org.miq.infinity.spc.domain.TraderInputCriteriaImpl;
import org.miq.infinity.spc.entity.SpcComputationOutputStore;
import org.miq.infinity.spc.util.SpcUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

@Repository
public class SpcComputationOutputRepositoryImpl implements SpcComputationOutputRepository {

    @Autowired
    private SpcComputationOutputDao spcComputationOutputDao;

    @Autowired
    private SpcUtil spcUtil;

    private static final String DATA_SUCCESS = "SUCCESS";
    private static final String DATA_ERROR = "ERROR";

    @Override
    public String getRecommendationIdentifier() {
        return spcComputationOutputDao.findMaxRecommendationIdentifier();
    }

    @Override
    public void updateSpcOutput(int isActive) {

        List<SpcComputationOutputStore> spcList = spcComputationOutputDao.findAll();

        Iterator<SpcComputationOutputStore> iterator = spcList.iterator();

        while (iterator.hasNext()) {
            SpcComputationOutputStore spcoutputstore = iterator.next();

            if (spcoutputstore.getIsActive() != 0) {
                spcoutputstore.setIsActive(isActive);
            }
        }
        spcComputationOutputDao.save(spcList);
    }

    @Override
    public List<SpcComputationOutputStore> getAllSpcOutputList(int isActive) {
        return spcComputationOutputDao.findByIsActive(isActive);

    }

    @Override
    public String saveSpcOutput(String spcOutput, int profileId, int isActive,
            TraderInputCriteriaImpl traderinputcriteria, int updatedRecommendationId) {

        isActive = isActive + 1;

        Gson gson = new Gson();
        Date creationDate = new Date();

        List<SpcComputationOutputStore> spccomputationoutputlist = new ArrayList<>();

        List<SpcComputationOutputCriteriaImpl> spcComputationoutput = gson.fromJson(spcOutput,
                new TypeToken<List<SpcComputationOutputCriteriaImpl>>() {
                }.getType());

        Iterator<SpcComputationOutputCriteriaImpl> iterator = spcComputationoutput.iterator();

        while (iterator.hasNext()) {
            SpcComputationOutputStore spcoutputstore = new SpcComputationOutputStore();
            SpcComputationOutputCriteria spcComputationcriteria = iterator.next();

            spcoutputstore.setDomain(spcComputationcriteria.getDomain());
            spcoutputstore.setExpectedCtr(spcComputationcriteria.getExpectedCtr());
            spcoutputstore.setExpectedImpressions(spcComputationcriteria.getExpectedImpressions());
            spcoutputstore.setExpectedViewability(spcComputationcriteria.getExpectedViewability());
            spcoutputstore.setRecommendationIdentifier(updatedRecommendationId);
            spcoutputstore.setRecommendedCpm(spcComputationcriteria.getRecommendedCpm());
            spcoutputstore.setDeviceType(spcComputationcriteria.getDeviceType());
            spcoutputstore.setProfileId(profileId);
            spcoutputstore.setCampaignId(traderinputcriteria.getCampaignId());
            spcoutputstore.setCampaignType(traderinputcriteria.getCampaignType());
            spcoutputstore.setCreationDate(creationDate);
            spcoutputstore.setIsActive(isActive);

            spccomputationoutputlist.add(spcoutputstore);
        }
        if (spcUtil.isCollectionEmpty(spccomputationoutputlist)) {
            return DATA_ERROR;
        }
        spcComputationOutputDao.save(spccomputationoutputlist);
        return DATA_SUCCESS;
    }

}
