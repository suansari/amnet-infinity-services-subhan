package org.miq.infinity.spc.repository;

import org.miq.infinity.spc.entity.ProfileInputStore;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProfileInputStoreDao extends JpaRepository<ProfileInputStore, Long> {

    public ProfileInputStore findByprofileName(String profileName);

}
