package org.miq.infinity.spc.controller.test;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.miq.infinity.spc.domain.TraderInputCriteriaImpl;
import org.miq.infinity.spc.engine.SpcProfileService;
import org.miq.infinity.spc.engine.SpcRecommendationOutputService;
import org.miq.infinity.spc.engine.SpcTraderService;
import org.miq.infinity.spc.repository.TraderInputRepository;
import org.miq.infinity.spc.util.SpcUtil;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
@ContextConfiguration
public class SpcTraderServiceTest {

    @Mock
    private SpcProfileService spcProfileService;

    @Mock
    private SpcUtil spcUtil;

    @InjectMocks
    SpcTraderService spcTraderService;

    @Mock
    TraderInputRepository traderInputRepository;

    @Mock
    SpcRecommendationOutputService spcRecommendationOutputService;

    TraderInputCriteriaImpl traderInput;

    @Before
    public void init() {
        Date startDate = new Date();

        List<String> creativeSize = new ArrayList<>();
        creativeSize.add("300*500");

        traderInput = new TraderInputCriteriaImpl();
        traderInput.setAdvertiserId(1234);
        traderInput.setBudget(9000);
        traderInput.setCampaignId(4567);
        traderInput.setCampaignType("Retargetting");
        traderInput.setCtrGoal("0.02");
        traderInput.setCreativeSize(creativeSize);
        traderInput.setDeviceType("Desktop");
        traderInput.setImpressions(900000);
        traderInput.setStartdate(startDate);
        traderInput.setEnddate(startDate);

    }

    @Test
    public void uploadTraderSucessTest() {
        String profileName = "sunil.com";
        int profileId = 1;
        String creativeSize = traderInput.getCreativeSize().toString();
        String status = "Data saved successfully";

        Mockito.when(spcProfileService.getProfileId(profileName)).thenReturn(profileId);

        Mockito.when(traderInputRepository.saveTraderInput(traderInput, profileId, creativeSize)).thenReturn(status);

        spcTraderService.uploadTraderInput(traderInput);
    }
}
