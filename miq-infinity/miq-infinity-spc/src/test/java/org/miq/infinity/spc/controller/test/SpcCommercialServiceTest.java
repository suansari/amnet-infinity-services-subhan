package org.miq.infinity.spc.controller.test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.miq.infinity.spc.controller.SpcController;
import org.miq.infinity.spc.domain.CommInputCreteriaImpl;
import org.miq.infinity.spc.engine.SpcCommercialService;
import org.miq.infinity.spc.engine.SpcProfileService;
import org.miq.infinity.spc.entity.CommercialInputStore;
import org.miq.infinity.spc.exception.ResourceNotFoundException;
import org.miq.infinity.spc.repository.CommercialInputRepository;
import org.miq.infinity.spc.util.SpcUtil;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import com.fasterxml.jackson.databind.ObjectMapper;

@RunWith(SpringRunner.class)
@SpringBootTest
@ContextConfiguration
public class SpcCommercialServiceTest {

    @Mock
    private SpcProfileService spcProfileService;

    @Mock
    private SpcUtil spcUtil;

    @Mock
    CommercialInputRepository commercialInputRepository;

    @InjectMocks
    SpcCommercialService spc;

    private static final Log LOGGER = LogFactory.getLog(SpcController.class);

    List<CommercialInputStore> commList1 = null;

    @Before
    public void init() {

        commList1 = new ArrayList<CommercialInputStore>();

        CommercialInputStore commInput = new CommercialInputStore();

        Date createDate = new Date();
        commInput.setDomain("Test.com");
        commInput.setPriority(1);
        commInput.setProfileId(10);
        commInput.setCreateDate(createDate);
        commInput.setUpdateDate(createDate);
        commInput.setIsActive(1);

        commList1.add(commInput);
    }

    // Checking for getting commercial input list is not null
    @Test
    public void spcListNotNullExampleTest() {
        List<CommercialInputStore> commList1 = new ArrayList<CommercialInputStore>();
        CommercialInputStore commInput = new CommercialInputStore();

        Date createDate = new Date();
        commInput.setDomain("Test.com");
        commInput.setPriority(1);
        commInput.setProfileId(10);
        commInput.setCreateDate(createDate);
        commInput.setUpdateDate(createDate);
        commInput.setIsActive(1);

        commList1.add(commInput);

        Mockito.when(commercialInputRepository.getCommInputList()).thenReturn(commList1);

        List<CommercialInputStore> commList2 = spc.getAllCommercialInput();

        org.junit.Assert.assertNotNull(commList2);
        ;

        System.out.println("Listing output is" + commList2.toString());
    }

    @Test
    public void spcListNullExampleTest() {

        List<CommercialInputStore> commList = null;

        Mockito.when(commercialInputRepository.getCommInputList()).thenReturn(null);

        Mockito.when(spcUtil.isCollectionEmpty(commList)).thenReturn(true);

        List<CommercialInputStore> commList2 = spc.getAllCommercialInput();

        // Assert.notNull(commList2);

        org.junit.Assert.assertNull(commList2);
    }

    @Test(expected = ResourceNotFoundException.class)
    public void spcListExceptionExampleTest() {

        List<CommercialInputStore> commList = null;
        Mockito.when(commercialInputRepository.getCommInputList()).thenReturn(commList);

        Mockito.when(spcUtil.isCollectionEmpty(commList)).thenReturn(true);

        Mockito.when(commercialInputRepository.getCommInputList())
                .thenThrow(new ResourceNotFoundException("Commercial list not found"));
        spc.getAllCommercialInput();
    }

    @Test
    public void FileUploadTestSuccess() throws Exception {
        String profileName = "sunil.com";
        int profileId = 1;
        String successStatus = "file upload successfull";

        MockMultipartFile uploadfile = new MockMultipartFile("Commfile", "testCommInput.xlsx", "text/plain",
                "Spring Framework".getBytes());

        Mockito.when(spcProfileService.getProfileId(profileName)).thenReturn(profileId);

        Mockito.when(spcUtil.parseFileAndreturnCommercialInputsList(uploadfile, profileId)).thenReturn(commList1);

        Mockito.when(spcUtil.isCollectionEmpty(commList1)).thenReturn(false);

        Mockito.when(commercialInputRepository.saveCommInputData(commList1)).thenReturn(successStatus);

        String status = spc.saveCommInput(uploadfile, profileName);

        org.junit.Assert.assertEquals(successStatus, status);
    }

    @Test
    public void FileUploadTestFail() throws Exception {
        String profileName = "sunil.com";
        int profileId = 1;
        String failStatus = "Error in uploading and saving file";

        MockMultipartFile uploadfile = new MockMultipartFile("Commfile", "testCommInput.xlsx", "text/plain",
                "Spring Framework".getBytes());

        Mockito.when(spcProfileService.getProfileId(profileName)).thenReturn(profileId);

        Mockito.when(spcUtil.parseFileAndreturnCommercialInputsList(uploadfile, profileId)).thenReturn(commList1);

        Mockito.when(spcUtil.isCollectionEmpty(commList1)).thenReturn(true);

        Mockito.when(commercialInputRepository.saveCommInputData(commList1)).thenReturn(failStatus);

        String status = spc.saveCommInput(uploadfile, profileName);

        org.junit.Assert.assertEquals(failStatus, status);
    }

    @Test(expected = Exception.class)
    public void FileUploadTestException() throws Exception {
        String profileName = "sunil.com";
        int profileId = 1;

        MockMultipartFile uploadfile = new MockMultipartFile("Commfile", "testCommInput.xlsx", "text/plain",
                "Spring Framework".getBytes());

        Mockito.when(spcProfileService.getProfileId(profileName)).thenReturn(profileId);

        Mockito.when(spcUtil.parseFileAndreturnCommercialInputsList(uploadfile, profileId)).thenThrow(Exception.class);

        spc.saveCommInput(uploadfile, profileName);
    }

    @Test
    public void commInputDeleteTestSuccess() {
        int id = 1;

        String deletionMessage = "Data is deleted successfully";

        Mockito.when(commercialInputRepository.deleteCommercialRecord(id)).thenReturn(deletionMessage);

        String status = spc.deleteCommInput(id);

        org.junit.Assert.assertEquals(deletionMessage, status);
    }

    @Test
    public void commInputUpdateTest() throws IOException {

        ObjectMapper objectMapper = new ObjectMapper();

        Date updationDate = new Date();

        CommInputCreteriaImpl commInputCriteria = new CommInputCreteriaImpl();
        commInputCriteria.setDomain("sunil.com");
        commInputCriteria.setPriority(2);
        commInputCriteria.setId(1);

        CommercialInputStore updatedOutput = new CommercialInputStore();

        updatedOutput.setId(1);
        updatedOutput.setDomain("sunil.com");
        updatedOutput.setUpdateDate(updationDate);

        String JsonUpdatedOutput = objectMapper.writeValueAsString(updatedOutput);

        Mockito.when(commercialInputRepository.updateCommercialRecord(commInputCriteria)).thenReturn(JsonUpdatedOutput);

        spc.updateCommercialInput(commInputCriteria);

        org.junit.Assert.assertNotNull(JsonUpdatedOutput);

    }
}
