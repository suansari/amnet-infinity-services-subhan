package org.miq.infinity.spc.controller.test;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.engine.internal.Nullability;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.miq.infinity.spc.domain.TraderInputCriteriaImpl;
import org.miq.infinity.spc.engine.SpcCommercialService;
import org.miq.infinity.spc.engine.SpcProfileService;
import org.miq.infinity.spc.engine.SpcRecommendationOutputService;
import org.miq.infinity.spc.engine.SpcTraderService;
import org.miq.infinity.spc.entity.CommercialInputStore;
import org.miq.infinity.spc.entity.SpcComputationOutputStore;
import org.miq.infinity.spc.exception.DataNotFoundException;
import org.miq.infinity.spc.exception.ResourceNotFoundException;
import org.miq.infinity.spc.repository.SpcComputationOutputRepository;
import org.miq.infinity.spc.repository.TraderInputRepository;
import org.miq.infinity.spc.util.SpcUtil;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.env.Environment;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@RunWith(SpringRunner.class)
@SpringBootTest
@ContextConfiguration
public class SpcComputationServiceTest {

    @Mock
    private SpcProfileService spcProfileService;

    @Mock
    private SpcUtil spcUtil;

    @Mock
    SpcCommercialService spcCommercialService;

    @Mock
    Environment environment;

    @Mock
    SpcComputationOutputRepository spcComputationOutputRepository;

    @InjectMocks
    SpcRecommendationOutputService spcRecommendationOutputService;

    TraderInputCriteriaImpl traderInput;

    List<CommercialInputStore> commList;

    @Before
    public void init() {
        Date startDate = new Date();

        List<String> creativeSize = new ArrayList<>();
        creativeSize.add("300*500");

        traderInput = new TraderInputCriteriaImpl();
        traderInput.setAdvertiserId(1234);
        traderInput.setBudget(9000);
        traderInput.setCampaignId(4567);
        traderInput.setCampaignType("Retargetting");
        traderInput.setCtrGoal("0.02");
        traderInput.setCreativeSize(creativeSize);
        traderInput.setDeviceType("Desktop");
        traderInput.setImpressions(900000);
        traderInput.setStartdate(startDate);
        traderInput.setEnddate(startDate);

        commList = new ArrayList<CommercialInputStore>();

        CommercialInputStore commInput = new CommercialInputStore();

        Date createDate = new Date();
        commInput.setDomain("Test.com");
        commInput.setPriority(1);
        commInput.setProfileId(10);
        commInput.setCreateDate(createDate);
        commInput.setUpdateDate(createDate);
        commInput.setIsActive(1);

        commList.add(commInput);

    }

    @Test
    public void computeTraderInputTest() throws DataNotFoundException, JsonProcessingException {
        int profileId = 1;

        String scriptPath = "/home/sunil/test.py";

        ObjectMapper objectMapper = new ObjectMapper();

        String commInputToJson = objectMapper.writeValueAsString(commList);
        String traderInputToJson = objectMapper.writeValueAsString(traderInput);

        String spcOutput = "SPC_OUTPUT";

        Mockito.when(environment.getProperty("script.path")).thenReturn(scriptPath);

        Mockito.when(spcCommercialService.getAllCommercialInput()).thenReturn(commList);

        Mockito.when(spcUtil.computeSpcAlgorithm(commInputToJson, traderInputToJson, scriptPath)).thenReturn(spcOutput);

        spcRecommendationOutputService.computeTraderInput(traderInput, profileId);

        org.junit.Assert.assertNotNull(spcOutput);

    }

    @Test(expected = NullPointerException.class)
    public void computeTraderInputExceptionTest() throws DataNotFoundException, JsonProcessingException {
        int profileId = 1;

        String scriptPath = "/home/sunil/test.py";

        ObjectMapper objectMapper = new ObjectMapper();

        String commInputToJson = objectMapper.writeValueAsString(commList);
        String traderInputToJson = objectMapper.writeValueAsString(traderInput);

        // String spcOutput= "SPC_OUTPUT";

        Mockito.when(environment.getProperty("script.path")).thenReturn(scriptPath);

        Mockito.when(spcCommercialService.getAllCommercialInput()).thenReturn(commList);

        Mockito.when(spcUtil.computeSpcAlgorithm(commInputToJson, traderInputToJson, scriptPath))
                .thenThrow(new NullPointerException());

        spcRecommendationOutputService.computeTraderInput(traderInput, profileId);
    }

    @Test
    public void parseSpcOutputTest() {
        String recommendationId = "1";
        int updatedRecommendationId = Integer.parseInt(recommendationId);
        String spcOutput = "SPC_OUTPUT";
        int profileId = 1;
        int isActive = 1;
        String status = "SUCCESS";
        Mockito.when(spcComputationOutputRepository.getRecommendationIdentifier()).thenReturn(recommendationId);
        Mockito.when(spcComputationOutputRepository.saveSpcOutput(spcOutput, profileId, isActive, traderInput,
                updatedRecommendationId)).thenReturn(status);

        spcRecommendationOutputService.parseSpcOutput(spcOutput, profileId, traderInput);
    }

    @Test(expected = Exception.class)
    public void parseSpcOutputExceptionTest() {
        String recommendationId = "1";
        int updatedRecommendationId = Integer.parseInt(recommendationId);
        String spcOutput = "SPC_OUTPUT";
        int profileId = 1;
        int isActive = 1;
        Mockito.when(spcComputationOutputRepository.getRecommendationIdentifier()).thenReturn(recommendationId);
        Mockito.when(spcComputationOutputRepository.saveSpcOutput(spcOutput, profileId, isActive, traderInput,
                updatedRecommendationId)).thenThrow(new Exception());

        spcRecommendationOutputService.parseSpcOutput(spcOutput, profileId, traderInput);
    }

    @Test
    public void gellAllSpcListTest() {
        int isActive = 1;
        List<SpcComputationOutputStore> spcList = new ArrayList<SpcComputationOutputStore>();
        SpcComputationOutputStore spcOutput = new SpcComputationOutputStore();

        spcList.add(spcOutput);

        Mockito.when(spcComputationOutputRepository.getAllSpcOutputList(1)).thenReturn(spcList);

        List<SpcComputationOutputStore> spcListOutput = spcRecommendationOutputService.getAllSpcOutput(isActive);

        org.junit.Assert.assertNotNull(spcListOutput);
    }

}
