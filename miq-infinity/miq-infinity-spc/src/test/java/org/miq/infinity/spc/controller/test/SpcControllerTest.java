package org.miq.infinity.spc.controller.test;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.miq.infinity.spc.controller.SpcController;
import org.miq.infinity.spc.domain.CommInputCreteriaImpl;
import org.miq.infinity.spc.domain.TraderInputCriteriaImpl;
import org.miq.infinity.spc.engine.SpcCommercialService;
import org.miq.infinity.spc.engine.SpcRecommendationOutputService;
import org.miq.infinity.spc.engine.SpcTraderService;
import org.miq.infinity.spc.entity.CommercialInputStore;
import org.miq.infinity.spc.entity.SpcComputationOutputStore;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SpcControllerTest {

    private static final Log LOGGER = LogFactory.getLog(SpcControllerTest.class);

    public static final MediaType APPLICATION_JSON_UTF8 = new MediaType(MediaType.APPLICATION_JSON.getType(),
            MediaType.APPLICATION_JSON.getSubtype(), Charset.forName("utf8"));

    private MockMvc mockMvc;

    @Mock
    SpcCommercialService spcCommercialService;

    @Mock
    SpcTraderService spcTraderService;

    @Mock
    SpcRecommendationOutputService spcRecommendationOutputService;

    @InjectMocks
    SpcController spcController;

    static final class Uri {
        private Uri() {
        }

        public static final String SPC = "/spc";
        public static final String COMM_LIST = SPC + "/media-owners/priority/list";
        public static final String COMM_INPUT = SPC + "/media-owners/add";
        public static final String COMM_INPUT_UPDATE = SPC + "/media-owners/update";
        public static final String COMM_INPUT_DELETE = SPC + "/media-owners/delete";
        public static final String TRADER_INPUT = SPC + "/campaigns/media-owners/add";
        public static final String SPC_OUTPUT_LIST = SPC + "/campaigns/media-owners/recommendation/list";
    }

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(spcController).build();
    }

    @Test
    public void getCommercialListTest() throws Exception {
        List<CommercialInputStore> commList = new ArrayList<CommercialInputStore>();

        CommercialInputStore commInput = new CommercialInputStore();

        Date createDate = new Date();
        commInput.setDomain("Test.com");
        commInput.setPriority(1);
        commInput.setProfileId(10);
        commInput.setCreateDate(createDate);
        commInput.setUpdateDate(createDate);
        commInput.setIsActive(1);

        commList.add(commInput);

        Mockito.when(spcCommercialService.getAllCommercialInput()).thenReturn(commList);

        mockMvc.perform(get(Uri.COMM_LIST).contentType(APPLICATION_JSON_UTF8)).andExpect(status().isOk());

    }

    // @Test
    // public void uploadCommInputTest() throws Exception
    // {
    //
    // String fileName="testCommInput.xlsx";
    // MockMultipartFile uploadfile = new MockMultipartFile("Commfile", fileName, "text/plain",
    // "Spring Framework".getBytes());
    //
    // String profileName ="sunil.com";
    // String status="file uploaded successfully";
    //
    // Mockito.when(spcCommercialService.saveCommInput(uploadfile, profileName)).thenReturn(status);
    //
    //// mockMvc.perform(post(Uri.COMM_INPUT).f)
    //// .andExpect(status().isOk());
    //
    // mockMvc.perform(MockMvcRequestBuilders.fileUpload(Uri.COMM_INPUT)
    // .file(uploadfile)
    // .param("fileName",fileName)
    // .param("profilename",profileName))
    // .andExpect(status().is(200));
    // }

    @Test
    public void updateCommercialInputTest() throws Exception {
        CommInputCreteriaImpl commInputCreteriaImplInput = new CommInputCreteriaImpl();

        commInputCreteriaImplInput.setDomain("a.com");
        commInputCreteriaImplInput.setId(2);
        commInputCreteriaImplInput.setPriority(3);

        ObjectMapper mapper = new ObjectMapper();

        String input = mapper.writeValueAsString(commInputCreteriaImplInput);

        String commInputCreteriaImplOutput = "UPDATE_OUTPUT";

        Mockito.when(spcCommercialService.updateCommercialInput(commInputCreteriaImplInput))
                .thenReturn(commInputCreteriaImplOutput);

        mockMvc.perform(put(Uri.COMM_INPUT_UPDATE).contentType(APPLICATION_JSON_UTF8).content(input))
                .andExpect(status().isOk());
    }

    @Test
    public void deleteCommercialInputTest() throws Exception {
        int id = 1;
        String status = "Delete Success";

        Mockito.when(spcCommercialService.deleteCommInput(id)).thenReturn(status);

        mockMvc.perform(delete(Uri.COMM_INPUT_DELETE).contentType(APPLICATION_JSON_UTF8).param("id", "1"))
                .andExpect(status().isOk());
    }

    @Test
    public void uploadTraderInputTest() throws Exception {
        Date startDate = new Date();

        List<String> creativeSize = new ArrayList<>();
        creativeSize.add("300*500");

        TraderInputCriteriaImpl traderInput = new TraderInputCriteriaImpl();
        traderInput.setAdvertiserId(1234);
        traderInput.setBudget(9000);
        traderInput.setCampaignId(4567);
        traderInput.setCampaignType("Retargetting");
        traderInput.setCtrGoal("0.02");
        traderInput.setCreativeSize(creativeSize);
        traderInput.setDeviceType("Desktop");
        traderInput.setImpressions(900000);
        traderInput.setStartdate(startDate);
        traderInput.setEnddate(startDate);

        ObjectMapper mapper = new ObjectMapper();

        String input = mapper.writeValueAsString(traderInput);

        String status = "SUCCESS";

        Mockito.when(spcTraderService.uploadTraderInput(traderInput)).thenReturn(status);

        mockMvc.perform(post(Uri.TRADER_INPUT).contentType(APPLICATION_JSON_UTF8).content(input))
                .andExpect(status().isOk());
    }

    @Test
    public void getSpcOutputListTest() throws Exception {
        List<SpcComputationOutputStore> spcList = new ArrayList<SpcComputationOutputStore>();

        SpcComputationOutputStore spcOutput = new SpcComputationOutputStore();

        Date createDate = new Date();
        int isActive = 1;

        spcOutput.setCampaignId(1234);
        spcOutput.setCampaignType("rtg");
        spcOutput.setCreationDate(createDate);
        spcOutput.setDomain("a.com");
        spcOutput.setExpectedCtr(0.03);
        spcOutput.setExpectedImpressions(90000);
        spcOutput.setExpectedViewability(">10%");
        spcOutput.setId(1);
        spcOutput.setIsActive(1);
        spcOutput.setProfileId(2);

        spcList.add(spcOutput);

        Mockito.when(spcRecommendationOutputService.getAllSpcOutput(isActive)).thenReturn(spcList);

        mockMvc.perform(get(Uri.SPC_OUTPUT_LIST).contentType(APPLICATION_JSON_UTF8)).andExpect(status().isOk());
    }

}
